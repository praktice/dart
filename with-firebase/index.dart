import 'dart:html';

import 'package:firebase/firebase.dart' as fb;
import 'package:firebase/src/assets/assets/dart';

main() async {
  await config();

  fb.initializeApp(
    apiKey: apiKey,
    authDomain: authDomain,
    databaseUrl: databaseUrl,
    storageBucket, storageBucket
  )


}
