import 'dart:html';

InputElement todoInput;
DivElement uiList;
ButtonElement buttonClear;

List<Todo> todoList = [];

// Single Todo Item
class Todo {
  // Increment the ID every time a new Todo is created
  // Used as a key so we know what to delete
  int id = 0;
  final String text;

  // Constructor
  Todo(
    this.text,
  ) {
    id++;
  }
}

void main() {
  // Select DOM elements from index.html
  todoInput = querySelector('#todo');
  uiList = querySelector('#todo-list');
  buttonClear = querySelector('#clear');

  // Listen for Events
  todoInput.onChange.listen(addTodo);
  buttonClear.onClick.listen(removeAllTodos);
}

void addTodo(Event event) {
  // Get the Value
  Todo todo = Todo(todoInput.value);
  // Add to Interal List
  todoList.add(todo);

  // Update List
  updateTodos();

  // Clear Input field
  todoInput.value = '';
}

void updateTodos() {
  // Clear All Todos
  uiList.children.clear();

  // Re-Add from List, (Prevent Duplicates)
  todoList.forEach((todo) {
    // Create a new Element for every Todo Item
    DivElement div = Element.div();
    ButtonElement buttonRemove = ButtonElement();
    Element span = Element.span();

    // Give Attribute to Element
    buttonRemove.text = 'X';
    buttonRemove.id = todo.id.toString();
    buttonRemove.onClick.listen(removeTodo);

    // Give Attribute to Element
    span.text = todo.text;

    // Give Attribute to Element
    div.children.add(buttonRemove);
    div.children.add(span);

    // Add to the ui (index.html)
    uiList.children.add(div);
  });
}

void removeTodo(MouseEvent event) {
  // Don't let the button do anything extra.
  event.stopPropagation();

  // Create Element
  Element div = (event.currentTarget as Element).parent;
  Element button = (event.currentTarget as Element);

  // Give it a unique key, remove a "-" if one is made, parse back to int
  int key = int.parse(button.id.split('-')[0]);

  // Remove from internal List
  todoList.removeWhere((todo) => todo.id == key);

  // Update the ui (index.html)
  div.remove();
}

void removeAllTodos(MouseEvent event) {
  // Clear all Children in the uiList (index.html)
  uiList.children.clear();

  // Clear the internal List
  todoList.clear();
}
