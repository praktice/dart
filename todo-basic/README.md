# build

```sh
stagehand web simple
pub get

# After editing Code
pub global activate webdev
webdev build
webdev serve
```

An absolute bare-bones web app.

Created from templates made available by Stagehand under a BSD-style
[license](https://github.com/dart-lang/stagehand/blob/master/LICENSE).
