# Dart Help

[TOC]

## Pub: Package Manager

How is this used?

```sh
pub get <package>
```


## Dart2JS: CLI

Usage:

```sh
dart2js <options>
```

- `-o <file> or --out=<file>` - Generates the output into <file>. If not specified, the output goes in a file named out.js.
- `--enable-asserts` - Enables assertion checking.
- `-O{0|1|2|3|4}`
  - Controls optimizations that can help reduce code size and improve performance of the generated code. For more details on these optimizations, `run dart2js -hv`.
  - `-O0`: Disables many optimizations.
  - `-O1`: Enables default optimizations.
  - `-O2`: Enables -O1 optimizations, plus additional ones (such as minification) that respect the language semantics and are safe for all programs.
  - `-O3`: Enables -O2 optimizations, plus omits implicit type checks.
  - `-O4`: Enables more aggressive optimizations than `-O3`, but with the same assumptions.

```sh
dart2js -02 -o out.js in.dart
```

- `--enable-diagnostic-colors` - Adds colors to diagnostic messages.
- `--show-package-warnings` - Shows warnings and hints generated from packages.
- `-D<flag>=<value>` - Defines an environment variable.

- `-o <file>` or `--out=<file>` - Generates the output into `<file>`. If not specified, the output goes in a file named `out.js`.
- `--enable-asserts` - Enables assertion checking.

---

Jesse Boyer
