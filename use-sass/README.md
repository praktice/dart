# Use SASS

## Create YML

**pubspec.yml**

```yml
name: my_project
dev_dependencies:
  sass: ^1.22.8
```

## Create Compile Sass

```sh
import 'dart:io';
import 'package:sass/sass.dart' as sass;

void main(List<String> arguments) {
  var result = sass.compile(arguments[0]);
  new File(arguments[1]).writeAsStringSync(result);
}
```

## Run It

```sh
dart compile-sass.dart styles.scss styles.css
```
