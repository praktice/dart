void main() {

  int height = 190;
  if (height > 150) {
    print( "You are taller than average" );
  } else {
    print ( "You are shorter than average" );
  }

  // Ternary
  int a = 10;
  String result2 = a > 5 ? " A is greater than 5 " : " A is not greater than 5";

  print(result2);
}
