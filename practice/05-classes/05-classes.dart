class Myself {
  String something = 'Hello';
  String name;

  Myself(String name) {
    this.name = name;
    Printable(this.name);
  }

  Printable(String s) {
    print(s);
  }
}

void main() {
  new Myself('Jesse');
}
