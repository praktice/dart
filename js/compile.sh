#!/bin/bash


if [ ! -d dist ]; then
  mkdir dist
fi

dart2js -o dist/main.js ./main.dart
