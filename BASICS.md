# Dart Basics

- [Dart Basics](#dart-basics)
  - [Concepts](#concepts)
  - [Really Basic](#really-basic)
  - [Variables](#variables)
  - [Built in Types](#built-in-types)
  - [Operators](#operators)
  - [If/Else](#ifelse)
  - [Switch/Case](#switchcase)
  - [Loops](#loops)
    - [For](#for)
    - [Iterables, For In](#iterables-for-in)
    - [While](#while)
- [Classes](#classes)
  - [Methods](#methods)
  - [Setters/Getters](#settersgetters)
  - [Abstract Methods](#abstract-methods)
  - [Abstract Classes](#abstract-classes)
  - [Interfaces](#interfaces)
  - [Extend](#extend)
  - [Override](#override)
  - [Mixins](#mixins)
  - [Static Variables](#static-variables)
  - [Static Methods](#static-methods)
  - [Generics](#generics)
- [Test](#test)
  - [Assert](#assert)
  - [Exception](#exception)
  - [Try/Catch](#trycatch)

## Concepts

- Everything is an **Object**.
- **Strong Type is Optional**, use `var` if you want.
- `List<int>` for only Integers
- `List<dynamic>` for Mixed List
- **There is no**: `public, protected, private`:
  - `_identifier` - **"\_"** Represents private

## Really Basic

- Top Level Execution (**required**):
  - `void main() { }`
- Comment:
  - `// comment`
- Data Type
  - `int/num/double, String, etc..`
- Number Literal:
  - `42`
- Show Output:
  - `print()`
- String Literal:
  - `'...'` or `"---"`
- String Interpolation:
  - `'hi $varname'` or `'hi ${expression}'`
- Auto-Figure Type:
  - `var name = 'Value';`

## Variables

- `int age = 25;`
- `final name = 'jesse';`
- `final String name = 'jesse';`
- `var anything = [1,2,3];`
- `const x = 10.1;`
- `const double y = 12.2;`
- `const foo = []`

## Built in Types

- `numbers`
  - `num a = 10;`
  - `int a = 10;`
  - `double a = 10.1;`
- `strings`
  - `var s1 = 'hello';`
  - `String s2 = "world";`
- `booleans`
  - `true` / `false`
- `lists`
  - `[1, 2, 3, 4];`
- `sets`
  - `(1, 1, 1, 2, 3)` is (`1, 2, 3)`
- `maps`
  - `{'key': 'value'}`
- `runes` - (for expressing Unicode - `characters` in a string)
  - `# something`
- `symbols`
  - `...`

## Operators

- Spread:
  - `...`
- Spread (Null aware):
  - `...?`
- Collection if:
  - `var a = [1, 2, if (1 > 2) 'yes']`;

## If/Else

```dart
if (isRaining()) {
  you.bringRainCoat();
} else if (isSnowing()) {
  you.wearJacket();
} else {
  car.putTopDown();
}
```

## Switch/Case

```dart
var command = 'OPEN';
switch (command) {
  case 'CLOSED':
    print('closed')
    break;
  case 'OPEN':
    print('open')
    break;
  default:
    print('unknown')
}
```

## Loops

- You can use the following:
  - `break`
  - `continue`

### For

```dart
var message = StringBuffer('Dart is fun');

for (var i = 0; i < 5; i++) {
  message.write('!');
}
```

### Iterables, For In

Iterables when you dont need the index:

```dart
candidates.forEach((candidate) => candidate.interview());

var collection = [0, 1, 2];
for (var x in collection) {
  print(x); // 0 1 2
}
```

### While

```scss
while (!isDone()) {
  doSomething();
}
do {
  printLine();
} while (!atEndOfPage());
```

# Classes

todo

## Methods

...

## Setters/Getters

...

## Abstract Methods

...

## Abstract Classes

...

## Interfaces

...

## Extend

...

## Override

...

## Mixins

...

## Static Variables

...

## Static Methods

...

## Generics

...

# Test

## Assert

```dart
assert(number < 100);
```

## Exception

```dart
throw 'anything';
throw FormatException('Expected at least 1 section');
```

## Try/Catch

```dart
try {
  breedMoreLlamas();
} on OutOfLlamasException {
  buyMoreLlamas();
}
```
